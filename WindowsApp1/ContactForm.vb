﻿'  File ContactForm
'  Sample code was taken from:
'  'https://docs.microsoft.com/en-us/dotnet/visual-basic/developing-apps/programming/drives-directories-files/how-to-read-from-comma-delimited-text-files
' 
' Assignment #2
' Due Oct 11, 2017
'
' Requirement 1: Expand on this form to display information in database that displays the following fields
' First Name (TextBox)
' Last Name (TextBox)
' Street Number (TextBox)
' City (TextBox) 
' Province (TextBox)
' Country (TextBox)
' Postal Code  (TextBox)( https://stackoverflow.com/questions/16614648/canadian-postal-code-regex)
' Phone Number (TextBox)( http://www.visual-basic-tutorials.com/Tutorials/Strings/validate-phone-number-in-visual-basic.htm)
' email Address (TextBox)( https://stackoverflow.com/questions/1331084/how-do-i-validate-email-address-formatting-with-the-net-framework)
'
' Add Next and Previous Buttons to navigate through the database ( handle index 0 and max index)
' Display the current primary key of the database in a textbox or label
' Add a Status TextBox and dispaly any formatting errors that are encoutered, 
' If multiple errors exist only show last one.

' Requirement 2: Expand on the below example to create a import the contents of the CSV file 
' created in Assignment1, read the data into entity classes and save data to database.  
' After import Next and Prev buttons should work.
'
' TODO for Dan - add example of how to save data
'
' Please always try to write clean And readable code
' Here Is a good reference doc http://ricardogeek.com/docs/clean_code.html  
' Submit to Bitbucket under Assignment2

Imports System.Text.RegularExpressions
Public Class ContactForm

    Dim index As Integer = 0

    'hashtable sample code
    ' https://support.microsoft.com/en-ca/help/307933/how-to-work-with-the-hashtable-collection-in-visual-basic--net-or-in-v
    '
    Dim allData As New Dictionary(Of Integer, Customer)

    Dim RowIdxFirstName As Integer = 0
    'Public Property csvFile As String = "C:\Users\dpenny\Documents\Source\Repos\mcda5510_assignments\WindowsApp1\names.csv"

    Private Sub NextButton_Click(sender As Object, e As EventArgs) Handles bn_next.Click
        index = tb_index.Text

        Using context As New Model1

            Dim cust As New customer
            cust = context.customers.OrderByDescending(Function(x) x.Id).First()
            If index = cust.Id Then
                MsgBox("This is last record")
            Else
                UpdateDataForDB(index + 1)
            End If
        End Using

    End Sub

    Private Sub UpdateDataForDB(key As Integer)
        Dim cust As customer = allData(key)
        tb_index.Text = key
        tb_fname.Text = cust.firstName
        tb_lname.Text = cust.lastName
        tb_snum.Text = cust.streetNumber
        tb_city.Text = cust.city
        tb_province.Text = cust.province
        tb_country.Text = cust.country
        tb_postalcode.Text = cust.postalCode
        tb_phonenumber.Text = cust.phoneNumber
        tb_email.Text = cust.emailAddress
        Validation(tb_postalcode.Text, tb_phonenumber.Text, tb_email.Text)
    End Sub


    Private Sub CustomerContactForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadFromDatabase()
    End Sub

    Private Sub Validation(postalCode As String, phoneNumber As String, email As String)
        Dim postalCodeRegex As New Regex("^([a-zA-Z]\d[a-zA-Z]( )?\d[a-zA-Z]\d)$")
        If postalCodeRegex.IsMatch(postalCode) Then

        Else
            Status.Text = "Not a valid Postal Code"
        End If
        Dim phoneNumberRegex As New Regex("\(\d{3}\)\d{3}-\d{4}")
        If phoneNumberRegex.IsMatch(phoneNumber) Then

        Else
            Status.Text = "Not a valid Phone Number"
        End If
        Dim emailRegex As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        If emailRegex.IsMatch(email) Then

        Else
            Status.Text = "Not a valid Email"
        End If
    End Sub
    Private Sub LoadFromDatabase()
        Using context As New Model1
            Dim listOfCustomers = context.customers.ToList

            For Each cust As customer In listOfCustomers
                allData.Add(cust.Id, cust)
            Next
            Try
                UpdateDataForDB(context.customers.OrderBy(Function(x) x.Id).First().Id)
            Catch
                MsgBox("No Data in the database")
            End Try
        End Using


    End Sub

    Private Sub LoadFromCSVFile()
        Using MyReader As New Microsoft.VisualBasic.
                              FileIO.TextFieldParser(
                                csvimport.Text)
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(",")
            Dim currentRow As String()
            Dim pos As Integer
            pos = 1
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()
                    Dim rowData As New ArrayList
                    Using context As New Model1
                        Dim cust As New customer
                        cust.firstName = currentRow(0)
                        cust.lastName = currentRow(1)
                        cust.streetNumber = currentRow(2)
                        cust.city = currentRow(3)
                        cust.province = currentRow(4)
                        cust.country = currentRow(5)
                        cust.postalCode = currentRow(6)
                        cust.phoneNumber = currentRow(7)
                        cust.emailAddress = currentRow(8)
                        context.customers.Add(cust)
                        context.SaveChanges()
                        Validation(tb_postalcode.Text, tb_phonenumber.Text, tb_email.Text)
                    End Using
                Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                    MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
                End Try
            End While
            MsgBox("File Uploaded Successfully")
        End Using
        LoadFromDatabase()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles bn_prev.Click
        index = tb_index.Text

        Using context As New Model1
            Dim cust As New customer
            cust = context.customers.OrderBy(Function(x) x.Id).First()
            If index = cust.Id Then
                MsgBox("This is first record")
            Else
                UpdateDataForDB(index - 1)
            End If
        End Using

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles bn_browse.Click
        ImportCSVFile()
    End Sub

    Private Sub ImportCSVFile()
        Dim importFile As String = ""
        Dim fd As OpenFileDialog = New OpenFileDialog()
        fd.Title = "Select CSV file to import"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            importFile = fd.FileName
            csvimport.Text = fd.FileName
        End If
    End Sub
    Private Sub bn_upload_Click(sender As Object, e As EventArgs) Handles bn_upload.Click
        LoadFromCSVFile()
    End Sub
End Class

